const express = require("express");
const router = express.Router();
const AuthController = require("../controllers/AuthController");

router.post("/token", AuthController.postRefreshTOken);

module.exports = router;