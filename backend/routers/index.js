const express = require("express");
const router = express.Router();

router.use("/shop", require("./ShopRouter"));
router.use("/user", require("./UserRouter"));
router.use("/auth", require("./AuthRouter"));

router.get('/', (req, res) => {
    res.send('Home page');
});


module.exports = router;