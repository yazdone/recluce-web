const express = require("express");
const router = express.Router();
const shopController = require("../controllers/ShopController");
const tokenValidator = require("../middlewares/TokenValidator");

// shop main route, featured games and item suggest in this route
router.get("/", shopController.getMain);

router.get("/game", shopController.getGames);
router.get("/game/:game", shopController.getSpecificGame);
router.post("/game", tokenValidator.verifyToken, shopController.postGames);
router.put("/game/:id", tokenValidator.verifyToken, shopController.putUpdateGame);
router.delete("/game/:id", tokenValidator.verifyToken, shopController.deleteGame);

router.post("/item", tokenValidator.verifyToken, shopController.postItems);
router.get("/item/:item", shopController.getItemsDetails);
router.put("/item/:id", tokenValidator.verifyToken, shopController.putUpdateItem);
router.delete("/item/:id", tokenValidator.verifyToken, shopController.deleteItem);

module.exports = router;