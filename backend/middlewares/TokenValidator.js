require("dotenv").config();
const jwt = require("jsonwebtoken");

//TODO: seperate admins and users
// Middleware for token verification
function verifyToken(req, res, next) {
    // token could be with x-access-token or authorization
    let token = req.headers["x-access-token"] || req.headers["authorization"];
    // if token has been sent with authorization will starts with Bearer, we only need token.

    if (token) {
        if (token.startsWith("Bearer "))
            token = token.slice(7, token.length);
        jwt.verify(token, process.env.PUBLIC_KEY, (err, decoded) => {
            if (err) {
                return res.status(401).send({message: "Token is not valid"});
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.status(401).send({ message: "Token not provided" });
    }
}
module.exports = { verifyToken: verifyToken }