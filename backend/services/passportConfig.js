const passport = require("passport");
const localStrategy = require("passport-local");
const bcrypt = require("bcrypt");
const userModel = require("../models/UserModel");

function passportConfig() {
    passport.use(new localStrategy({ usernameField: "email" }, (email, password, done) => {
        userModel.findOne({ email: email }, (err, user) => {
            if (err) {
                return done(null, false, { message: "Invalid credentials." });
            } else {
                bcrypt.compare(password, user.password).then((result) => {
                    if (result) {
                        return done(null, user)
                    } else {
                        return done(null, false, { message: "Invalid credentials." });
                    }
                }).catch(err => done(err));
            }
        });
    }));

    passport.serializeUser((user, done) => {
        console.log("inside passport serializeUser user: " + user);
        return done(null, user.id);
    });

    passport.deserializeUser((userId, done) => {
        userModel.findOne({ _id: userId }, (err, result) => {
            if (err) {
                console.error(err);
            } else {
                console.log("inside passport deserializeUser user: " + result);
                return done(null, result)
            }
        });
    });

}
module.exports = passportConfig;