const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        min: 0,
        required: true
    },
    price: [{
        type: Number,
        required: true
    }],
    image: {
        type: Buffer,
        contentType: String,
        required: true
    },
    game: {
        type: mongoose.Schema.Types.ObjectId, ref: "Game"
    }
}, { timestamps: true });

const item = mongoose.model("Item", itemSchema);

module.exports = item;