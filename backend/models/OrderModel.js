const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    itemId: { type: mongoose.Schema.Types.ObjectId, ref: "Item" },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    no: { type: Number, require: true },
    totalPrice: { type: Number, require: true }
}, { timestamps: true });

const order = new mongoose.model("Order", orderSchema);

module.exports = order;