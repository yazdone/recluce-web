require("dotenv").config();
const jwt = require("jsonwebtoken");

// payload must be object literal
function sign(payload) {
    const tokenOptions = {
        expiresIn: 1800000,
        algorithm: "RS256"
    }
    return jwt.sign(payload, process.env.PRIVATE_KEY, tokenOptions);
}

function verify(token) {
    try {
        return jwt.verify(token, process.env.PUBLIC_KEY);
    } catch (err) {
        return false;
    }
}

function decodeUnverfied(token) {
    return jwt.decode(token, { complete: true });
}

module.exports = { sign: sign, verify: verify, decodeUnverfied: decodeUnverfied };