const fd = require("formidable");

const form = fd({ maxFileSize: (1024 * 1024) });

function formidable(req) {
    return new Promise((resolve, reject) => {
        form.parse(req, (err, field, File) => {
            if (err) {
                reject(err)
            } else {
                resolve([field, File]);
            }
        });
    });
}

module.exports = { formidable: formidable }