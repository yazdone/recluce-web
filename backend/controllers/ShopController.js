const gameModel = require("../models/GameModel");
const itemModel = require("../models/ItemModel");
const formidable = require("../libs/Formidable");
const fs = require("fs");

function getMain(req, res) {
}

function getGames(req, res) {
    gameModel.find((err, games) => {
        if (err) {
            res.json({ success: false });
        } else {
            res.json({ success: true, games: games });
        }
    });
}

function getSpecificGame(req, res) {
    gameModel.findOne({ name: req.params.game }, (err, result) => {
        if (err) {
            res.status(500).send({ message: "Problem with getting data from Data Base" });
        } else if (result) {
            res.status(200).send(result);
        } else {
            res.status(404).send({ message: "Can't find any game with this name, try somthing else." });
        }
    });
}

async function putUpdateGame(req, res) {
    if (req.decoded.acl === "admin") {
        const [field, file] = await formidable.formidable(req)
            .catch((err) => {
                res.status(400).json({ message: "Problem with reciving fields, file, also be sure file size lower than 1MB " + err });
                process.exit(1);
            });

        fs.readFile(file.image.path, "base64", (err, data) => {
            if (err) {
                res.status(500).json({ message: "we have porblem to reading Image file." });
            } else {
                gameModel.findById({ _id: req.params.id }, (err, result) => {
                    if (err) {
                        res.status(500).json({ message: "Problem with getting data from Data Base" });
                    } else if (result) {
                        const game = {
                            name: field.name,
                            image: data
                        }

                        gameModel.updateOne({ _id: req.params.id }, game, (err, game) => {
                            if (err) {
                                res.status(400).json({ message: err });
                            } else {
                                res.status(200).json({ message: "Game updated successfuly." });
                            }
                        });
                    } else {
                        res.status(400).json({ message: "Game not found for creating new item" });
                    }
                });
            }
        });
    } else {
        res.status(401).send({ message: "Forbidden!" });
    }
}

function deleteGame(req, res) {
    if (req.decoded.acl === "admin") {
        gameModel.deleteOne({ _id: req.params.id }, (err, result) => {
            if (err) {
                res.status(500).send({ message: "Problem with deleting game" });
            } else if (result.deletedCount === 0) {
                res.status(404).send({ message: "Item does'nt exists." });
            } else {
                res.status(200).send({ message: "Game deleted successfully" });
            }
        });
    } else {
        res.status(401).send({ message: "Forbidden!" });
    }
}

/* Game database has these entities: name, image. both required.
 */
async function postGames(req, res) {
    if (req.decoded.acl === "admin") {
        const [field, file] = await formidable.formidable(req)
            .catch((err) => {
                res.status(400).json({ message: "Problem with reciving fields, file, also be sure file size lower than 1MB " + err });
                process.exit(1);
            });

        gameModel.findOne({ name: field.name }, (err, game) => {
            if (!game) {
                fs.readFile(file.image.path, "base64", (err, data) => {
                    if (err) {
                        res.status(500).json({ message: "we have porblem to reading Image file." });
                    } else {
                        const newGame = new gameModel({
                            name: field.name,
                            image: {
                                data: data,
                                contentType: file.image.type
                            }
                        });
                        newGame.save().then(game => {
                            res.status(201).json({ message: "New game added Successfully" });
                        }).catch(err => {
                            res.status(500).json({ message: "Problem in saving image into DB" });
                        });
                    }
                });
            } else {
                res.status(400).json({ message: "This game already exist." });
            }
        });
    } else {
        res.status(401).send({ message: "Forbidden!" });
    }

}

async function postItems(req, res) {
    if (req.decoded.acl === "admin") {
        const [field, file] = await formidable.formidable(req)
            .catch((err) => {
                res.status(400).json({ message: "Problem with reciving fields, file, also be sure file size lower than 1MB " + err });
                process.exit(1);
            });

        itemModel.findOne({ name: field.name }, (err, item) => {
            if (!item) {
                const price = [];
                if (field.priceUS) {
                    price.push(field.priceUS);

                    if (field.priceIR) {
                        price.push(field.priceIR);
                    }
                } else {
                    res.status(400).json({ message: "US price is necessary" });
                }
                fs.readFile(file.image.path, "base64", (err, data) => {
                    if (err) {
                        res.status(500).json({ message: "we have porblem to reading Image file." });
                    } else {
                        gameModel.findOne({ name: field.game }, (err, game) => {
                            if (game) {
                                const newItem = new itemModel({
                                    name: field.name,
                                    quantity: field.quantity,
                                    price: price,
                                    image: data,
                                    game: game.id
                                });

                                newItem.save((err) => {
                                    if (err) {
                                        res.status(500).json({ message: "Problem in saving image into DB" });
                                    } else {
                                        res.status(201).json({ message: "New item added successfully" });
                                    }
                                });
                            } else {
                                res.status(400).json({ message: "Game not found for creating new item" });
                            }
                        });
                    }
                });
            } else {
                res.status(400).json({ message: "This item already exists" });
            }
        });
    } else {
        res.status(401).send({ message: "Forbidden!" });
    }

}


function getItemsDetails(req, res) {
    itemModel.findOne({ name: req.params.item }, (err, itemDetails) => {
        if (err) {
            console.log(err);
        } else if (itemDetails) {
            res.status(200).json({ item: itemDetails });
        } else {
            res.status(404).json({ message: "Can't find any Item with this name, try somthing else." });
        }
    });
}

async function putUpdateItem(req, res) {
    if (req.decoded.acl === "admin") {
        const [field, file] = await formidable.formidable(req)
            .catch((err) => {
                res.status(400).json({ message: "Problem with reciving fields, file, also be sure file size lower than 1MB " + err });
                process.exit(1);
            });

        const price = [];
        if (field.priceUS) {
            price.push(field.priceUS);

            if (field.priceIR) {
                price.push(field.priceIR);
            }
        } else {
            res.status(400).json({ message: "US price is necessary" });
        }
        fs.readFile(file.image.path, "base64", (err, data) => {
            if (err) {
                res.status(500).json({ message: "we have porblem to reading Image file." });
            } else {
                gameModel.findOne({ name: field.game }, (err, game) => {
                    if (game) {
                        const newItemData = ({
                            name: field.name,
                            quantity: field.quantity,
                            price: price,
                            image: data,
                            game: game.id
                        });

                        itemModel.updateOne({ _id: req.params.id }, newItemData, (err, result) => {
                            if (err) {
                                res.status(400).json({ message: err });
                            } else {
                                res.status(200).json({ message: "Item updated successfuly." });
                            }
                        });

                    } else {
                        res.status(400).json({ message: "Game not found for creating new item" });
                    }
                });
            }
        });
    } else {
        res.status(401).send({ message: "Forbidden!" });
    }

}

function deleteItem(req, res) {
    if (req.decoded.acl === "admin") {
        itemModel.deleteOne({ _id: req.params.id }, (err, result) => {
            if (err) {
                res.status(500).send({ message: err });
            } else if (result.deletedCount === 0) {
                res.status(404).send({ message: "Item does'nt exists." });
            } else {
                res.status(200).send({ message: "Item deleted successfully." });
            }
        });
    } else {
        res.status(401).send({ message: "Forbidden!" });
    }

}
module.exports = {
    getMain: getMain
    , postGames: postGames
    , getGames: getGames
    , postItems: postItems
    , getItemsDetails: getItemsDetails
    , putUpdateItem: putUpdateItem
    , deleteItem: deleteItem
    , getSpecificGame: getSpecificGame
    , putUpdateGame: putUpdateGame
    , deleteGame: deleteGame
}