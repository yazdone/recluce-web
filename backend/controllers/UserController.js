const userModel = require("../models/UserModel");
const bcrypt = require("bcrypt");
const JWTService = require("../libs/JWT");

function postSignUp(req, res) {
    //todo: validate email and password

    userModel.findOne({ email: req.body.email }, (err, result) => {
        if (err) {
            console.log(err)
        } else if (!result) {
            bcrypt.hash(req.body.password, 14).then((result) => {
                const newUser = new userModel({
                    email: req.body.email,
                    password: result
                });

                newUser.save((err) => {
                    if (err) {
                        console.error(err);
                    } else {
                        res.status(201).send({ message: "Account has  been created" });
                    }
                });

            }).catch(err => console.error(err));
        } else {
            res.status(400).send({ message: "Duplicate email" });
        }
    });
}

function postSignIn(req, res) {
    userModel.findOne({ email: req.body.email }, (err, user) => {
        if (err) {
            res.status(500).send({ message: "DataBase error" });
        } else if(user){
            bcrypt.compare(req.body.password, user.password).then((result) => {
                if (result) {
                    const payload = { userId: user.id, acl: "user" }
                    const token = JWTService.sign(payload);
                    res.status(200).send({ token: token, expireTime: (Date.now() / 1000) + (86400 * 30) }); // Token expiration time is set on a month
                } else {
                    res.status(401).send({ message: "Email or Password is incorrect" });
                }
            }).catch((err)=> {
                console.error(err);
            });
        }else{
            res.status(401).send({ message: "Email or Password is incorrect" });
        }
    });
}

module.exports = {
    postSignUp: postSignUp,
    postSignIn: postSignIn
}