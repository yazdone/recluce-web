const JWTService = require("../libs/JWT");

function postRefreshTOken(req, res) {

    const verify = JWTService.verify(req.body.token);
    console.log(verify);
    if (!verify) {
        res.json({ success: false, message: "Invalid token or user" });
    } else {
        console.log(verify);
    }
}

module.exports = { postRefreshTOken: postRefreshTOken }