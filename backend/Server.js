require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const app = new express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({extended : true}));
// parse requests of content-type - application/json
app.use(express.json());
mongoose.connect(process.env.DB_ADDRESS,
	{ useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set("useCreateIndex", true);

// configure session middleware

/* app.use(expressSession({
	secret: process.env.COOkIE_SECRET,
	resave: false,
	saveUninitialized: true,
	store: new mongoStore({ mongooseConnection: mongoose.connection }),
})); */


// applying passport and session configurations
/* app.use(passport.initialize());
app.use(passport.session()); */

// require all routes which gathered in /routers/index.js
app.use(require("./routers/Index"));

app.listen(process.env.PORT, () => console.log("Server running"));